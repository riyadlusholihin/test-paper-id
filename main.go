package main

import (
	"fmt"
	"net/http"
	"test-paper-id/app/adapter"
	"test-paper-id/app/controller"
	"test-paper-id/app/router"
	"test-paper-id/app/usecase"

	"github.com/sirupsen/logrus"
)

const (
	SERVER_PORT = "8080"
)

func main() {
	adapter.MySqll()

	usecase := usecase.NewUC()
	ctrl := controller.NewControllers(usecase)

	route := router.NewRouter(ctrl)
	router := route.Router()

	logrus.Infof("[SERVER] starting server in port :%v", SERVER_PORT)
	logrus.Fatalln(http.ListenAndServe(fmt.Sprintf(":%v", SERVER_PORT), router))
}
