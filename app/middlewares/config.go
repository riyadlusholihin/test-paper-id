package middlewares

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"test-paper-id/app/models"
	"time"

	"github.com/dgrijalva/jwt-go"
)

var LOGIN_EXPIRATION_DURATION = time.Duration(10) * time.Hour
var JWT_SIGNING_METHOD = jwt.SigningMethodHS256
var JWT_SIGNATURE_KEY = []byte("testDoang")

type Response struct {
	Status       bool        `json:"status"`
	StatusCode   int         `json:"status_code"`
	ErrorMessage string      `json:"error_message"`
	Data         interface{} `json:"data"`
}

func Auth(f http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		cookie := &http.Cookie{}
		authorization, err := r.Cookie("auth")
		if err != nil {
			response(w, http.StatusUnauthorized, "Unauthorization")
			return
		}
		cookie = authorization

		if cookie.Value == "" {
			response(w, http.StatusUnauthorized, "Need Authorization")
			return
		}

		claims := &models.TokenClaimsAndCreate{}
		token, err := jwt.ParseWithClaims(cookie.Value, claims, func(t *jwt.Token) (interface{}, error) {
			if method, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
				log.Print("while t.method")
				return nil, fmt.Errorf("Invalid Token")
			} else if method != JWT_SIGNING_METHOD {
				return nil, fmt.Errorf("InvalidSinged")
			}

			return JWT_SIGNATURE_KEY, nil
		})

		if err != nil {
			log.Printf("while error parse : %v", err)
			response(w, http.StatusUnauthorized, "Invalid Token")
			return
		}

		if !token.Valid {
			log.Print("while Jwt.map claims")
			response(w, http.StatusUnauthorized, "Invalid Token")
			return
		}

		byt, err := json.Marshal(claims)
		if err != nil {
			log.Print("valid marshal")
			response(w, http.StatusBadRequest, "Opps, Something Wrong")
		}

		r.Header.Set("Authorization", string(byt))

		reqs := r.WithContext(ctx)
		f.ServeHTTP(w, reqs)
	})
}

func response(w http.ResponseWriter, code int, data interface{}) {
	var (
		response Response
	)

	response.ErrorMessage = data.(string)
	response.Status = false
	response.StatusCode = code

	generateResponse(w, code, &response)
}

func generateResponse(w http.ResponseWriter, code int, res *Response) {
	respons, _ := json.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	fmt.Fprint(w, string(respons))
}
