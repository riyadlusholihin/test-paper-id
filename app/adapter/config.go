package adapter

import (
	"fmt"
	"test-paper-id/app/models"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func MySqll() {
	username := "root"
	password := ""
	database := "paper"
	connection, err := gorm.Open(mysql.Open(fmt.Sprintf("%s:%s@/%s?parseTime=true", username, password, database)), &gorm.Config{})
	if err != nil {
		panic("Could not Connect to the database")
	}

	DB = connection

	connection.AutoMigrate(&models.User{}, &models.Transaction{})
}
