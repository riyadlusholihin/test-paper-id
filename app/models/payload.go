package models

type Payload struct {
	Id           int    `json:"id"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	JenisKelamin string `json:"jenis_kelamin"`
	NoHP         string `json:"no_hp"`

	//payload
	Keterangan string `json:"keterangan"`
	Amount     int    `json:"amount"`
	StartDate  string `json:"start_date"`
	EndDate    string `json:"end_date"`
}
