package models

type User struct {
	Id           string `json:"id"`
	Username     string `json:"username" gorm:"unique"`
	Password     string `json:"password"`
	JenisKelamin string `gorm:"size:2"`
	NoHP         string `gorm:"size:19"`
}
