package models

import "time"

type Transaction struct {
	Id         int       `json:"id" gorm:"id"`
	Userid     string    `json:"-" gorm:"size:191"`
	Keterangan string    `json:"keterangan" gorm:"size:100"`
	Amount     int       `json:"amount" gorm:"amount"`
	Dates      string    `json:"date" gorm:"size:15"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	DeletedAt  time.Time `json:"-"`
}

type ResponseTransaction struct {
	Id         int    `json:"id"`
	Userid     string `json:"-"`
	Keterangan string `json:"keterangan"`
	Amount     int    `json:"amount"`
	Date       string `json:"date"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
	DeletedAt  string `json:"-"`
}
