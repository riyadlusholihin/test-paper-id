package models

import "github.com/dgrijalva/jwt-go"

type TokenClaimsAndCreate struct {
	jwt.StandardClaims
	Id       string `json:"id"`
	Username string `json:"username"`
}
