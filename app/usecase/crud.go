package usecase

import (
	"errors"
	"fmt"
	"math"
	"net/http"
	"test-paper-id/app/adapter"
	"test-paper-id/app/models"
	"time"
)

func (uc *uc) Create(auth *models.User, payload *models.Payload) (string, int, error) {
	var (
		message = "Berhasil Menambahkan Data"
		code    = http.StatusOK
	)
	if payload.Keterangan == "" {
		return "Keterangan tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	} else if payload.Amount == 0 {
		return "Amount tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	}
	now := time.Now()

	save := models.Transaction{
		Userid:     auth.Id,
		Keterangan: payload.Keterangan,
		Amount:     payload.Amount,
		Dates:      now.Format(TimeFormat2),
		CreatedAt:  now.Add(time.Hour * 7),
		UpdatedAt:  now.Add(time.Hour * 7),
	}

	result := adapter.DB.Create(&save)
	if result.Error != nil {
		message = "Gagal Menambahkan Data"
		code = http.StatusBadRequest
		return message, code, result.Error
	}

	return message, code, nil
}

func (uc *uc) All(auth *models.User, query string, page int) (interface{}, string, int, error) {
	var (
		data    []models.ResponseTransaction
		message = "Anda Belum Memiliki Transaction"
		code    = http.StatusOK
	)
	var total int64
	perPage := 9

	adapter.DB.Raw(query).Count(&total)

	query = fmt.Sprintf("%s LIMIT %d OFFSET %d", query, perPage, (page-1)*perPage)

	rows, err := adapter.DB.Raw(query).Rows()
	if err != nil {
		return nil, "Gagal Mengambil data", http.StatusBadRequest, err
	}

	for rows.Next() {
		var (
			trns models.Transaction
			resp models.ResponseTransaction
		)
		err = adapter.DB.ScanRows(rows, &trns)
		if err != nil {
			return nil, "Gagal mengambil data", http.StatusInternalServerError, err
		}

		resp.Id = trns.Id
		resp.Userid = trns.Userid
		resp.Keterangan = trns.Keterangan
		resp.Amount = trns.Amount
		resp.Date = trns.Dates
		resp.CreatedAt = trns.CreatedAt.Format(TimeFormat)
		resp.UpdatedAt = trns.UpdatedAt.Format(TimeFormat)

		data = append(data, resp)
	}

	res := map[string]interface{}{
		"data":      data,
		"total":     total,
		"page":      page,
		"last_page": math.Ceil(float64(total) / float64(perPage)),
	}

	return res, message, code, nil
}

func (uc *uc) Read(auth *models.User, query string) (interface{}, string, int, error) {
	var (
		data    models.ResponseTransaction
		message = "Data Tidak ditemukan"
		code    = http.StatusOK
	)

	rows, err := adapter.DB.Raw(query).Rows()
	if err != nil {
		return nil, "Gagal Mengambil data", http.StatusBadRequest, err
	}

	for rows.Next() {
		var (
			trns models.Transaction
			resp models.ResponseTransaction
		)
		err = adapter.DB.ScanRows(rows, &trns)
		if err != nil {
			return nil, "Gagal mengambil data", http.StatusInternalServerError, err
		}

		resp.Id = trns.Id
		resp.Userid = trns.Userid
		resp.Keterangan = trns.Keterangan
		resp.Amount = trns.Amount
		resp.Date = trns.Dates
		resp.CreatedAt = trns.CreatedAt.Format(TimeFormat)
		resp.UpdatedAt = trns.UpdatedAt.Format(TimeFormat)

		data = resp
	}

	if data.Id == 0 {
		return nil, message, http.StatusNotFound, errors.New("not found")
	}

	return data, "", code, nil
}

func (uc *uc) Update(auth *models.User, payload *models.Payload, id string) (string, int, error) {
	var (
		message     = "Berhasil Update Data"
		code        = http.StatusOK
		transcation models.Transaction
	)
	now := time.Now()

	update := models.Transaction{
		Keterangan: payload.Keterangan,
		Amount:     payload.Amount,
		UpdatedAt:  now,
	}

	result := adapter.DB.Model(&transcation).Where("id = ? AND userid = ?", id, auth.Id).Updates(&update)
	if result.Error != nil {
		message = "Gagal Mengupdate Data"
		code = http.StatusBadRequest
		return message, code, result.Error
	}

	return message, code, nil
}

func (uc *uc) Delete(auth *models.User, id string) (string, int, error) {
	var (
		message     = "Berhasil Hapus Data"
		code        = http.StatusOK
		transcation models.Transaction
	)

	if query := adapter.DB.Where("userid = ? AND id = ?", auth.Id, id).First(&transcation); query.Error != nil {
		return "Data Tidak ditemukan", http.StatusNotFound, query.Error
	}

	now := time.Now()

	update := models.Transaction{
		DeletedAt: now,
	}

	result := adapter.DB.Model(&transcation).Where("id = ? AND userid = ?", id, auth.Id).Updates(&update)
	if result.Error != nil {
		message = "Gagal Menghapus Data"
		code = http.StatusBadRequest
		return message, code, result.Error
	}

	return message, code, nil
}
