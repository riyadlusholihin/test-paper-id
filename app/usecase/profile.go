package usecase

import (
	"net/http"
	"strings"
	"test-paper-id/app/adapter"
	"test-paper-id/app/models"
)

func (uc *uc) Profile(auth *models.User) (interface{}, string, int, error) {
	var (
		users  models.User
		data   models.User
		jenkel string
	)

	if query := adapter.DB.Where("id = ?", auth.Id).First(&users); query.Error != nil {
		return nil, "Username Tidak ditemukan", http.StatusNotFound, query.Error
	}

	Jnis := strings.ToUpper(users.JenisKelamin)
	if Jnis == "M" {
		jenkel = "Male"
	} else if Jnis == "F" {
		jenkel = "Female"
	} else {
		jenkel = Jnis
	}

	data.Id = users.Id
	data.Username = users.Username
	data.JenisKelamin = jenkel
	data.NoHP = users.NoHP

	return data, "", http.StatusOK, nil
}
