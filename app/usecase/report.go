package usecase

import (
	"errors"
	"fmt"
	"net/http"
	"test-paper-id/app/adapter"
	"test-paper-id/app/models"
	"time"
)

func (uc *uc) Report(auth *models.User, payload *models.Payload) (interface{}, string, int, error) {
	var (
		data         []models.ResponseTransaction
		jam_start    = "00:00:00"
		jam_end      = "01:01:01"
		Total_Amount int
	)

	if payload.StartDate == "" {
		return nil, "StartDate tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	} else if payload.EndDate == "" {
		return nil, "EndDate tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	}

	start, _ := time.Parse("2006-01-02 15:04:05", payload.StartDate+" "+jam_start)
	end, _ := time.Parse("2006-01-02 15:04:05", payload.EndDate+" "+jam_end)
	if start.Before(end) {
		query := fmt.Sprintf("SELECT * FROM transactions WHERE userid = '%s' AND deleted_at = '0000-00-00 00:00:00.000' AND dates BETWEEN '%s' AND '%s' ORDER BY id DESC", auth.Id, payload.StartDate, payload.EndDate)
		rows, err := adapter.DB.Raw(query).Rows()
		if err != nil {
			return nil, "Gagal Mengambil data", http.StatusBadRequest, err
		}

		for rows.Next() {
			var (
				trns models.Transaction
				resp models.ResponseTransaction
			)
			err = adapter.DB.ScanRows(rows, &trns)
			if err != nil {
				return nil, "Gagal mengambil data", http.StatusInternalServerError, err
			}

			resp.Id = trns.Id
			resp.Userid = trns.Userid
			resp.Keterangan = trns.Keterangan
			resp.Amount = trns.Amount
			resp.Date = trns.Dates
			resp.CreatedAt = trns.CreatedAt.Format(TimeFormat)
			resp.UpdatedAt = trns.UpdatedAt.Format(TimeFormat)

			Total_Amount += trns.Amount

			data = append(data, resp)
		}
	} else {
		return nil, "End date tidak boleh lebih besar dari start date", http.StatusBadRequest, errors.New("Invalid")
	}

	res := map[string]interface{}{
		"name":              auth.Username,
		"print":             time.Now().Format(TimeFormat),
		"total_transaction": Total_Amount,
		"list_tansaction":   data,
	}

	return res, "", http.StatusOK, nil
}
