package usecase

import (
	"log"
	"net/http"
	"test-paper-id/app/adapter"
	"test-paper-id/app/middlewares"
	"test-paper-id/app/models"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

func (uc *uc) ValidateUser(user *models.Payload) (*models.User, string, int, error) {
	var (
		users models.User
	)

	if query := adapter.DB.Where("username = ?", user.Username).First(&users); query.Error != nil {
		return nil, "Username Tidak ditemukan", http.StatusNotFound, query.Error
	}

	err := bcrypt.CompareHashAndPassword([]byte(users.Password), []byte(user.Password))
	if err != nil {
		return nil, "Password salah", http.StatusBadRequest, err
	}

	return &users, "User Valid", http.StatusOK, nil
}

func (uc *uc) GenerateToken(user *models.User) (string, int, error) {
	var (
		message string
		code    int
	)

	claim := models.TokenClaimsAndCreate{
		StandardClaims: jwt.StandardClaims{
			Issuer:    user.Id,
			ExpiresAt: time.Now().Add(middlewares.LOGIN_EXPIRATION_DURATION).Unix(),
		},
		Id:       user.Id,
		Username: user.Username,
	}

	tokens := jwt.NewWithClaims(
		middlewares.JWT_SIGNING_METHOD,
		claim,
	)

	singedToken, err := tokens.SignedString(middlewares.JWT_SIGNATURE_KEY)
	if err != nil {
		log.Printf("Gagal Generate Token : %v", err)
		message = "Gagal Login"
		code = http.StatusBadRequest
		return message, code, err
	}

	return singedToken, http.StatusOK, nil
}
