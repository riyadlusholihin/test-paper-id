package usecase

import (
	"net/http"
	"test-paper-id/app/models"
)

const (
	TimeFormat  = "2006-01-02 15:04:05"
	TimeFormat2 = "2006-01-02"
)

type uc struct {
}

type UC interface {
	UserProcess(r *http.Request) (*models.User, *models.Payload, string, error)
	Register(user *models.Payload) (string, int, error)

	//login
	ValidateUser(user *models.Payload) (*models.User, string, int, error)
	GenerateToken(user *models.User) (string, int, error)

	//Crud
	Create(auth *models.User, payload *models.Payload) (string, int, error)
	All(auth *models.User, selects string, page int) (interface{}, string, int, error)
	Read(auth *models.User, query string) (interface{}, string, int, error)
	Update(auth *models.User, payload *models.Payload, id string) (string, int, error)
	Delete(auth *models.User, id string) (string, int, error)

	//profile
	Profile(auth *models.User) (interface{}, string, int, error)

	//report
	Report(auth *models.User, payload *models.Payload) (interface{}, string, int, error)
}

func NewUC() UC {
	return &uc{}
}
