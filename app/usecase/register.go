package usecase

import (
	"errors"
	"net/http"
	"strings"
	"test-paper-id/app/adapter"
	"test-paper-id/app/models"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func (uc *uc) Register(user *models.Payload) (string, int, error) {
	var jenKel string
	if user.Username == "" {
		return "Username tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	} else if user.Password == "" {
		return "Password tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	} else if user.JenisKelamin == "" {
		return "Jenis Kelamin tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	} else if user.NoHP == "" {
		return "No Handphone tidak boleh kosong", http.StatusBadRequest, errors.New("Invalid")
	}

	password, _ := bcrypt.GenerateFromPassword([]byte(user.Password), 14)

	jenisKelamin := strings.ToUpper(user.JenisKelamin)
	if jenisKelamin == "L" || jenisKelamin == "PRIA" || jenisKelamin == "MALE" || jenisKelamin == "M" {
		jenKel = "M"
	} else if jenisKelamin == "P" || jenisKelamin == "PEREMPUAN" || jenisKelamin == "WANITA" || jenisKelamin == "FEMALE" {
		jenKel = "F"
	} else {
		return "Jenis Kelamin tidak dikenali", http.StatusBadRequest, errors.New("Invalid")
	}

	register := models.User{
		Id:           uuid.New().String(),
		Username:     user.Username,
		JenisKelamin: jenKel,
		NoHP:         user.NoHP,
		Password:     string(password),
	}

	insert := adapter.DB.Create(&register)
	if insert.Error != nil {
		if strings.Contains(insert.Error.Error(), "Error 1062") {
			return "Username Sudah Pernah Digunakan", http.StatusConflict, insert.Error
		}
		return "Gagal Mendaftar", http.StatusBadRequest, insert.Error
	}

	return "Berhasil Mendaftar", http.StatusOK, nil
}
