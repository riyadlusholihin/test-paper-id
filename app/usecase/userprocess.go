package usecase

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"test-paper-id/app/models"
)

func (uc *uc) UserProcess(r *http.Request) (*models.User, *models.Payload, string, error) {
	var (
		pay  models.Payload
		auth models.User
	)

	authorization := r.Header.Get("Authorization")
	if authorization != "" {
		err := json.Unmarshal([]byte(authorization), &auth)
		if err != nil {
			log.Printf("Error Auth : %v", err)
			return nil, nil, "Incorrect Authorization", err
		}
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		return nil, nil, "Request invalid", err
	}

	err = json.Unmarshal(body, &pay)
	if err != nil {
		log.Printf("Error while unmarshal : %v", err)
		return nil, nil, "Request invalid", err
	}

	return &auth, &pay, "", nil
}
