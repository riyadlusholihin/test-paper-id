package router

import (
	"net/http"
	"test-paper-id/app/controller"
	"test-paper-id/app/middlewares"

	"github.com/go-chi/chi/v5"
)

type route struct {
	ctrl controller.Controllers
}

type Route interface {
	Router() http.Handler
}

func NewRouter(ctrl controller.Controllers) Route {
	return &route{ctrl: ctrl}
}

func (c *route) Router() http.Handler {
	router := chi.NewRouter()

	router.Group(func(r chi.Router) {
		r.Post("/api/register", c.ctrl.Register)
		r.Post("/api/login", c.ctrl.Login)
	})

	router.Group(func(r chi.Router) {
		r.Use(middlewares.Auth)
		r.Get("/api/all", c.ctrl.All)
		r.Post("/api/create", c.ctrl.Create)
		r.Get("/api/read/{id}", c.ctrl.Read)
		r.Put("/api/update/{id}", c.ctrl.Update)
		r.Delete("/api/delete", c.ctrl.Delete)

		//Report
		r.Post("/api/report", c.ctrl.Report)

		//user
		r.Get("/api/profile", c.ctrl.Profile)
		r.Post("/api/logout", c.ctrl.Logout)
	})

	return router
}
