package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"test-paper-id/app/usecase"
)

type ctrl struct {
	uc usecase.UC
}

type Controllers interface {
	Register(w http.ResponseWriter, r *http.Request)
	Login(w http.ResponseWriter, r *http.Request)
	Logout(w http.ResponseWriter, r *http.Request)

	//crud
	Create(w http.ResponseWriter, r *http.Request)
	All(w http.ResponseWriter, r *http.Request)
	Read(w http.ResponseWriter, r *http.Request)
	Update(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)

	//profile
	Profile(w http.ResponseWriter, r *http.Request)

	//Report
	Report(w http.ResponseWriter, r *http.Request)
}

func NewControllers(us usecase.UC) Controllers {
	return &ctrl{uc: us}
}

type Response struct {
	Status       bool        `json:"status"`
	StatusCode   int         `json:"status_code"`
	ErrorMessage string      `json:"error_message"`
	Data         interface{} `json:"data"`
}

func response(w http.ResponseWriter, status bool, code int, data interface{}) {
	var (
		response Response
	)

	if status {
		response.Data = data
		response.Status = status
		response.StatusCode = code
	} else {
		response.ErrorMessage = data.(string)
		response.Status = status
		response.StatusCode = code
	}

	generateResponse(w, code, &response)
}

func generateResponse(w http.ResponseWriter, code int, res *Response) {
	respons, _ := json.Marshal(res)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	fmt.Fprint(w, string(respons))
}
