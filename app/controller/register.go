package controller

import (
	"log"
	"net/http"
)

func (c *ctrl) Register(w http.ResponseWriter, r *http.Request) {
	_, pay, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	msg, code, err := c.uc.Register(pay)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("Error while Register : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, code, msg)
}
