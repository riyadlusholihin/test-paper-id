package controller

import (
	"log"
	"net/http"
	"time"
)

func (c *ctrl) Login(w http.ResponseWriter, r *http.Request) {
	_, user, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	data, msg, code, err := c.uc.ValidateUser(user)
	if code >= http.StatusBadRequest || err != nil {
		log.Printf("Error while Validate User : %v", err)
		response(w, false, code, msg)
		return
	}

	tokens, code, err := c.uc.GenerateToken(data)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("Error while GenerateToken : %v", err)
		response(w, false, code, tokens)
		return
	}

	cookie := http.Cookie{
		Name:     "auth",
		Value:    tokens,
		Expires:  time.Now().Add(time.Hour * 3),
		HttpOnly: true,
	}

	http.SetCookie(w, &cookie)

	response(w, true, code, "Berhasil Login")
}

func (c *ctrl) Logout(w http.ResponseWriter, r *http.Request) {
	auth, _, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	cookie := http.Cookie{
		Name:     "auth",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HttpOnly: true,
	}

	http.SetCookie(w, &cookie)

	res := map[string]interface{}{
		"User":    auth.Username,
		"Message": "Berhasil Logout",
	}

	response(w, true, http.StatusOK, res)
}
