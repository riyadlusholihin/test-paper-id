package controller

import (
	"log"
	"net/http"
)

func (c *ctrl) Report(w http.ResponseWriter, r *http.Request) {
	auth, pay, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	data, msg, code, err := c.uc.Report(auth, pay)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("Error while Report :%v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, code, data)
}
