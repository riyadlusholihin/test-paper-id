package controller

import (
	"log"
	"net/http"
)

func (c *ctrl) Profile(w http.ResponseWriter, r *http.Request) {
	auth, _, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	data, msg, code, err := c.uc.Profile(auth)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("error while all : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, code, data)
}
