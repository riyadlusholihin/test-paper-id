package controller

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

func (c *ctrl) Create(w http.ResponseWriter, r *http.Request) {
	auth, Pay, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	msg, code, err := c.uc.Create(auth, Pay)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("error while create : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, http.StatusOK, msg)
}

func (c *ctrl) All(w http.ResponseWriter, r *http.Request) {
	var (
		pages int
	)

	auth, _, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	master := fmt.Sprintf("SELECT * FROM transactions WHERE userid = '%s' AND deleted_at = '0000-00-00 00:00:00.000'", auth.Id)

	if s := r.URL.Query().Get("s"); s != "" {
		master = fmt.Sprintf("%s AND keterangan LIKE '%%%s%%'", master, s)
	}

	if sort := r.URL.Query().Get("sort"); sort != "" {
		master = fmt.Sprintf("%s ORDER BY created_at DESC, amount %s", master, sort)
	} else {
		master = fmt.Sprintf("%s ORDER BY created_at DESC", master)
	}

	page := r.URL.Query().Get("page")
	if page != "" {
		pages, _ = strconv.Atoi(page)
	} else {
		pages = 1
	}

	data, msg, code, err := c.uc.All(auth, master, pages)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("error while all : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, code, data)
}

func (c *ctrl) Read(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	auth, _, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	master := fmt.Sprintf("SELECT * FROM transactions WHERE userid = '%s' AND id = '%s' AND deleted_at = '0000-00-00 00:00:00.000'", auth.Id, id)

	data, msg, code, err := c.uc.Read(auth, master)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("error while all : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, code, data)
}

func (c *ctrl) Update(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	auth, Pay, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	msg, code, err := c.uc.Update(auth, Pay, id)
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("error while Update : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, http.StatusOK, msg)
}

func (c *ctrl) Delete(w http.ResponseWriter, r *http.Request) {
	auth, Pay, msg, err := c.uc.UserProcess(r)
	if err != nil {
		log.Printf("Error while user process :%v", err)
		response(w, false, http.StatusBadRequest, msg)
		return
	}

	msg, code, err := c.uc.Delete(auth, strconv.Itoa(Pay.Id))
	if err != nil || code >= http.StatusBadRequest {
		log.Printf("error while Delete : %v", err)
		response(w, false, code, msg)
		return
	}

	response(w, true, http.StatusOK, msg)
}
